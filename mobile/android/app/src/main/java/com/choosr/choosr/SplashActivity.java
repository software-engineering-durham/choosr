package com.choosr.choosr;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.choosr.choosr.models.ServerResponse;
import com.choosr.choosr.models.User;
import com.choosr.choosr.utils.SessionManager;
import com.choosr.choosr.utils.WebServices;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity {

    private WebServices service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

        decorView.setSystemUiVisibility(uiOptions);
        SessionManager manager = new SessionManager(SplashActivity.this);
        if(manager.isLoggedIn()) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WebServices.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(WebServices.class);

            GetUserInfoTask getUserInfoTask = new GetUserInfoTask(manager.getUserInfo().getId());
            getUserInfoTask.execute();

        } else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 2000);
        }
    }

    private class GetUserInfoTask extends AsyncTask<Void, Void, User> {

        private long userId;

        GetUserInfoTask(long userId){
            this.userId = userId;
        }

        @Override
        protected User doInBackground(Void... users) {
            return getUserProfile();
        }

        @Override
        protected void onPostExecute(User user) {
            SessionManager manager = new SessionManager(SplashActivity.this);
            if(user != null){
                manager.setUserInfo(user);
                manager.setLogin(true);
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            } else {
                manager.clearUserInfo();
                manager.setLogin(false);
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            finish();
        }

        private User getUserProfile(){
            try {
                Call<ServerResponse<User>> request =
                        service.getUserProfile(userId);
                Response<ServerResponse<User>> response = request.execute();
                ServerResponse<User> serverResponse = response.body();
                return serverResponse.getData();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
