package com.choosr.choosr.views;

import android.app.AlertDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.content.DialogInterface;
import android.widget.SpinnerAdapter;

import com.choosr.choosr.R;
import com.choosr.choosr.models.Category;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MultiSelectionSpinner extends android.support.v7.widget.AppCompatSpinner
        implements DialogInterface.OnMultiChoiceClickListener {
    Category[] items;
    boolean[] selections;

    ArrayAdapter<String> adapter;

    public MultiSelectionSpinner(Context context){
        super(context);
        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        adapter.add("No filters");
        super.setAdapter(adapter);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        adapter.add("No filters");
        super.setAdapter(adapter);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (selections != null && which < selections.length) {
            selections[which] = isChecked;

            adapter.clear();
            adapter.add(buildSelectedItem());
        } else {
            throw new IllegalArgumentException(
                    "Invalid which index");
        }
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCustomTitle(LayoutInflater.from(getContext()).inflate(R.layout.dialog_title, null, false));
        String[] sItems = new String[items.length];
        for(int i = 0; i<items.length;i++){
            sItems[i] = items[i].toString();
        }
        builder.setMultiChoiceItems(sItems, selections, this);
        builder.show();
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(Category[] items) {
        this.items = items;
        selections = new boolean[items.length];
        Arrays.fill(selections, false);
    }

    public void setItems(List<Category> items) {
        this.items = items.toArray(new Category[items.size()]);
        selections = new boolean[this.items.length];
        Arrays.fill(selections, false);
    }

    public void setSelection(Category[] selection) {
        for (Category cat : selection) {
            for (int j = 0; j < items.length; ++j) {
                if (items[j].getId() == cat.getId()) {
                    selections[j] = true;
                }
            }
        }
    }

    public void setSelection(List<Category> selection) {
        for (int i = 0; i < selections.length; i++) {
            selections[i] = false;
        }
        for (Category sel : selection) {
            for (int j = 0; j < this.items.length; ++j) {
                if (this.items[j].equals(sel)) {
                    selections[j] = true;
                }
            }
        }
        adapter.clear();
        adapter.add(buildSelectedItem());
    }

    public void setSelection(int index) {
        for (int i = 0; i < selections.length; i++) {
            selections[i] = false;
        }
        if (index >= 0 && index < selections.length) {
            selections[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index
                    + " is out of bounds.");
        }
        adapter.clear();
        adapter.add(buildSelectedItem());
    }

    public void setSelection(int[] selectedIndicies) {
        for (int i = 0; i < selections.length; i++) {
            selections[i] = false;
        }
        for (int index : selectedIndicies) {
            if (index >= 0 && index < selections.length) {
                selections[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index
                        + " is out of bounds.");
            }
        }
        adapter.clear();
        adapter.add(buildSelectedItem());
    }

    public List<Category> getSelectedCategories() {
        List<Category> selection = new LinkedList<>();
        for (int i = 0; i < items.length; ++i) {
            if (selections[i]) {
                selection.add(items[i]);
            }
        }
        return selection;
    }

    public List<Integer> getSelectedIndices() {
        List<Integer> selection = new LinkedList<>();
        for (int i = 0; i < items.length; ++i) {
            if (selections[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    private String buildSelectedItem() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;


        for (int i = 0; i < items.length; ++i) {
            if (selections[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;

                sb.append(items[i]);
            }
        }
        if(!foundOne){
            return "No filters";
        }
        return sb.toString();
    }
}
