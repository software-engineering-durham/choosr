package com.choosr.choosr;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.choosr.choosr.models.ServerResponse;
import com.choosr.choosr.models.User;
import com.choosr.choosr.utils.SessionManager;
import com.choosr.choosr.utils.WebServices;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    private UserRegisterTask rTask = null;

    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private EditText usernameEditText;
    private EditText firstnameEditText;
    private EditText lastnameEditText;
    private EditText addressEditText;
    private EditText phoneEditText;

    private View mProgressView;
    private View mRegisterView;

    private WebServices service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setBackgroundDrawableResource(R.drawable.login_background);

        emailEditText = findViewById(R.id.email);
        usernameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        confirmPasswordEditText = findViewById(R.id.confirm_password);
        firstnameEditText = findViewById(R.id.firstname);
        lastnameEditText = findViewById(R.id.lastname);
        addressEditText = findViewById(R.id.address);
        phoneEditText = findViewById(R.id.phonenumber);
        phoneEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button registerButton = findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mProgressView = findViewById(R.id.register_progress);
        mRegisterView = findViewById(R.id.register_form);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebServices.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(WebServices.class);

        TextView loginmessage = findViewById(R.id.loginmessage);
        String message = getString(R.string.login_message);
        loginmessage.setText(Html.fromHtml(message));
    }

    private void attemptLogin() {
        if (rTask != null) {
            return;
        }

        // Reset errors.
        emailEditText.setError(null);
        usernameEditText.setError(null);
        passwordEditText.setError(null);
        confirmPasswordEditText.setError(null);
        firstnameEditText.setError(null);
        lastnameEditText.setError(null);

        // Store values at the time of the login attempt.
        String email = emailEditText.getText().toString();
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String cpassword = confirmPasswordEditText.getText().toString();
        String firstname = firstnameEditText.getText().toString();
        String lastname = lastnameEditText.getText().toString();
        String address = addressEditText.getText().toString();
        String phone = phoneEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if(!TextUtils.isEmpty(phone) && !isPhoneValid(phone)){
            phoneEditText.setError(getString(R.string.error_invalid_phone));
            focusView = phoneEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastname)) {
            lastnameEditText.setError(getString(R.string.error_field_required));
            focusView = lastnameEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(firstname)) {
            firstnameEditText.setError(getString(R.string.error_field_required));
            focusView = firstnameEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(cpassword)) {
            confirmPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = confirmPasswordEditText;
            cancel = true;
        } else if (!cpassword.equals(password)){
            confirmPasswordEditText.setError(getString(R.string.error_invalid_confirm_password));
            focusView = confirmPasswordEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        } else if (!isPasswordValid(password)){
            passwordEditText.setError(getString(R.string.error_invalid_password));
            focusView = passwordEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            usernameEditText.setError(getString(R.string.error_field_required));
            focusView = usernameEditText;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            usernameEditText.setError(getString(R.string.error_invalid_username));
            focusView = usernameEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            focusView = emailEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            rTask = new UserRegisterTask(new User(email, username, firstname, lastname, address, phone, null),
                    password);
            rTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }

    private boolean isUsernameValid(String username){
        return username.length() >= 4 && username.matches("[a-zA-Z0-9]+");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private boolean isPhoneValid(String phone){
        return phone.matches("[0-9]+");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRegisterView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRegisterView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRegisterView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean>{

        private User user;
        private String password;

        UserRegisterTask(User user, String password){
            this.user = user;
            this.password = password;
        }

        @Override
        protected Boolean doInBackground(Void... users) {
            int id = registerUser();
            if(id > 0) {
                user.setId(id);
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            rTask = null;
            showProgress(false);
            if(response){
                Toast.makeText(getApplicationContext(), "User succesfully registered",
                        Toast.LENGTH_SHORT).show();
                SessionManager manager = new SessionManager(RegisterActivity.this);
                manager.setUserInfo(this.user);
                manager.setLogin(true);
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "There is a problem registering this user",
                        Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            rTask = null;
            showProgress(false);
        }

        private int registerUser(){
            try {
                Call<ServerResponse> request =
                        service.registerUser(user.getUsername(),
                                password,
                                user.getEmail(),
                                user.getFirstname(),
                                user.getLastname(),
                                user.getAddress(),
                                user.getPhone());
                Response<ServerResponse> response = request.execute();
                ServerResponse serverResponse = response.body();
                return serverResponse.getSuccess();
            } catch (IOException e) {
                e.printStackTrace();
                return 0;
            }

        }
    }


}
