package com.choosr.choosr.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.choosr.choosr.BR;
import com.choosr.choosr.R;
import com.choosr.choosr.utils.WebServices;
import com.google.gson.annotations.SerializedName;

public class User extends BaseObservable{
    @SerializedName("userID")
    private long id;
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("firstName")
    private String firstname;
    @SerializedName("lastName")
    private String lastname;
    @SerializedName("address")
    private String address;
    @SerializedName("phoneNo")
    private String phone;
    @SerializedName("image")
    private String urlimage;

    public User(long id, String email, String username, String firstname, String lastname,
                String address, String phone, String image) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.phone = phone;
        this.urlimage = image;
    }

    public User(User user) {
        this.id = user.id;
        this.email = user.email;
        this.username = user.username;
        this.firstname = user.firstname;
        this.lastname = user.lastname;
        this.address = user.address;
        this.phone = user.phone;
        this.urlimage = user.urlimage;
    }

    public User(String email, String username, String firstname,
                String lastname, String address, String phone, String urlimage) {
        this(0, email, username, firstname, lastname, address, phone, urlimage);
    }

    public User(int id, String email, String username, String firstname, String lastname, String image) {
        this(id, email, username, firstname, lastname, null, null, image);
    }

    public User(String email, String username, String firstname, String lastname, String image) {
        this(0, email, username, firstname, lastname, null, null, image);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Bindable
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
        notifyPropertyChanged(BR.firstname);
    }

    @Bindable
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
        notifyPropertyChanged(BR.lastname);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    public String getUrlimage() {
        return urlimage;
    }

    public void setUrlimage(String urlimage) {
        this.urlimage = urlimage;
    }

    @Override
    public String toString() {
        return String.format("%s %s [%s]", firstname, lastname, username);
    }

    @BindingAdapter({"android:profileImage"})
    public static void loadImage(ImageView view, String imageUrl) {
        if(imageUrl != null && !imageUrl.isEmpty()) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.logov)
                    .error(R.drawable.logov)
                    .priority(Priority.HIGH);
            Glide.with(view.getContext())
                    .load(WebServices.BASE_URL + imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public static void loadUriImage(ImageView view, Uri uri){

    }
}
