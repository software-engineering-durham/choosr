package com.choosr.choosr.utils;

import com.choosr.choosr.models.ServerResponse;
import com.choosr.choosr.models.User;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface WebServices {

    String BASE_URL = "https://community.dur.ac.uk/brian.k.isaac-medina/choosr/";

    @POST("login.php")
    @FormUrlEncoded
    Call<ServerResponse<User>> loginUser(@Field("user") String user,
                                         @Field("password") String password);

    @POST("register.php")
    @FormUrlEncoded
    Call<ServerResponse> registerUser(@Field("username") String username,
                                            @Field("password") String password,
                                            @Field("email") String email,
                                            @Field("firstname") String firstname,
                                            @Field("lastname") String lastname,
                                            @Field("address") String address,
                                            @Field("phoneNo") String phoneNo);

    @POST("profile.php")
    @FormUrlEncoded
    Call<ServerResponse<User>> getUserProfile(@Field("userid") long userid);

    @POST("updateuser.php")
    @Multipart
    Call<ServerResponse<User>> updateUserProfile(@Part("username") RequestBody username,
                                           @Part("firstname") RequestBody firstname,
                                           @Part("lastname") RequestBody lastname,
                                           @Part("address") RequestBody address,
                                           @Part("phoneNo") RequestBody phoneNo,
                                           @Part MultipartBody.Part file);
}
