package com.choosr.choosr.models;

import com.google.gson.annotations.SerializedName;

public class ServerResponse<T> {
    @SerializedName("success")
    private int success;

    @SerializedName("data")
    private T data;

    public int getSuccess() {
        return success;
    }

    public T getData() {
        return data;
    }
}
