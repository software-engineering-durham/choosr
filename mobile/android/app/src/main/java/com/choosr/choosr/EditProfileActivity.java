package com.choosr.choosr;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.choosr.choosr.databinding.ActivityEditProfileBinding;
import com.choosr.choosr.models.ServerResponse;
import com.choosr.choosr.models.User;
import com.choosr.choosr.utils.SessionManager;
import com.choosr.choosr.utils.WebServices;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfileActivity extends AppCompatActivity{

    private final int REQUEST_GALLERY_CODE = 200;
    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 201;
    private User originalUser;
    private User user;
    private ActivityEditProfileBinding binding;
    private boolean isPictureSet;
    private MultipartBody.Part fileToUpload = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        isPictureSet = false;

        binding = DataBindingUtil.setContentView(this,
                R.layout.activity_edit_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SessionManager sessionManager = new SessionManager(this);
        user = sessionManager.getUserInfo();
        originalUser = new User(user);



        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        binding.setUser(user);
        user.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                validateChanges();
            }
        });
        Handlers handlers = new Handlers(this, user, binding.fab);
        binding.setHandlers(handlers);

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Edit profile");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:{
                if(grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, REQUEST_GALLERY_CODE);
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK){
            Uri uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, EditProfileActivity.this);
            File file = new File(filePath);

            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            fileToUpload = MultipartBody.Part.createFormData("profilepicture", file.getName(), mFile);

            Bitmap bmImg = BitmapFactory.decodeFile(filePath);
            binding.profilepicture.setImageDrawable(null);
            binding.profilepicture.setImageBitmap(bmImg);
            binding.profilepicture.setScaleType(ImageView.ScaleType.CENTER_CROP);

            binding.clearpicturebutton.setVisibility(View.VISIBLE);
            isPictureSet = true;
            validateChanges();
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void validateChanges(){
        if(!user.getFirstname().equals(originalUser.getFirstname()) ||
                !user.getLastname().equals(originalUser.getLastname()) ||
                !user.getAddress().equals(originalUser.getAddress()) ||
                !user.getPhone().equals(originalUser.getPhone()) ||
                isPictureSet){
            binding.fab.setVisibility(View.VISIBLE);
        } else {
            binding.fab.setVisibility(View.GONE);
        }
    }

    public class Handlers{


        private Context context;
        private User user;
        private WebServices service;

        public Handlers(Context context, User user, View fab) {
            this.context = context;
            this.user = user;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WebServices.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(WebServices.class);
        }

        public void onFabClicked(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle("Confirm edit")
                    .setMessage("Are you sure you want to update your profile information?")
                    .setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new UpdateUserInfoTask(user).execute();
                        }
                    })
                    .setNegativeButton("Cancel", null);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        public void onUploadImageClicked(View view){
            if (ContextCompat.checkSelfPermission(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                ActivityCompat.requestPermissions(EditProfileActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            } else {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_GALLERY_CODE);
            }
        }

        public void onClearImageClicked(View view) {
            isPictureSet = false;
            binding.clearpicturebutton.setVisibility(View.GONE);
            if(originalUser.getUrlimage() != null && !originalUser.getUrlimage().isEmpty())
                User.loadImage(binding.profilepicture, originalUser.getUrlimage());
            else {
                binding.profilepicture.setImageDrawable(getDrawable(R.drawable.logov));
                binding.profilepicture.setScaleType(ImageView.ScaleType.CENTER);
            }
            validateChanges();
        }

        private class UpdateUserInfoTask extends AsyncTask<Void, Void, User> {

            private User user;

            UpdateUserInfoTask(User user){
                this.user = user;
            }

            @Override
            protected User doInBackground(Void... users) {
                return updateUser();
            }

            @Override
            protected void onPostExecute(User response) {
                SessionManager manager = new SessionManager(EditProfileActivity.this);
                if(response != null){
                    manager.setUserInfo(response);
                    setResult(ProfileActivity.PROFILE_EDITED);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error. Try again later.",
                            Toast.LENGTH_SHORT).show();
                }
            }

            private User updateUser(){
                try {
                    Log.d("USER", user.getUsername());

                    Call<ServerResponse<User>> request =
                            service.updateUserProfile(RequestBody.create(MediaType.parse("text/plain"), user.getUsername()),
                                    RequestBody.create(MediaType.parse("text/plain"), user.getFirstname()),
                                    RequestBody.create(MediaType.parse("text/plain"), user.getLastname()),
                                    RequestBody.create(MediaType.parse("text/plain"), user.getAddress()),
                                    RequestBody.create(MediaType.parse("text/plain"), user.getPhone()),
                                                        fileToUpload);
                    Response<ServerResponse<User>> response = request.execute();
                    ServerResponse<User> serverResponse = response.body();
                    Log.d("Server response", serverResponse.getSuccess() + "");
                    return serverResponse.getData();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
