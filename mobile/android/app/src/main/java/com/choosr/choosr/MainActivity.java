package com.choosr.choosr;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.choosr.choosr.models.Category;
import com.choosr.choosr.utils.SessionManager;
import com.choosr.choosr.views.MultiSelectionSpinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    private boolean showMap = false;
    private View advancedControls;
    private MultiSelectionSpinner spinner;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ToggleButton button = findViewById(R.id.advtoggle);
        button.setOnClickListener(this);
        advancedControls = findViewById(R.id.advancedsearch);
        spinner = findViewById(R.id.categories_spiner);
        spinner.setItems(mockCategories());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.toggle_map:{
                item.setIcon(showMap ? R.drawable.ic_map_black_24dp : R.drawable.ic_list_24dp);
                showMap = !showMap;
                return true;
            }
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.advtoggle:{
                if(advancedControls.getVisibility() == View.GONE){
                    advancedControls.setVisibility(View.VISIBLE);
                } else {
                    advancedControls.setVisibility(View.GONE);
                }
            }
            break;
            default:
                break;
        }
    }

    private List<Category> mockCategories(){
        List<Category> categories = new ArrayList<>();
        categories.add(new Category(1, "Vegan"));
        categories.add(new Category(2, "Vegetarian"));
        categories.add(new Category(3, "Pizza"));
        categories.add(new Category(4, "Gluten Free"));
        categories.add(new Category(5, "Salad"));
        categories.add(new Category(6, "Mexican"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));
        categories.add(new Category(7, "Chinese"));

        return categories;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(false);

        drawerLayout.closeDrawers();
        switch (item.getItemId()){
            case R.id.navigation_activity:{
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT)
                        .show();

            }
            break;
            case R.id.navigation_items:{
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT)
                        .show();

            }
            break;
            case R.id.navigation_account:{
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.navigation_chat:{
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT)
                        .show();

            }
            break;
            case R.id.navigation_logout:{
                SessionManager sessionManager = new SessionManager(this);
                sessionManager.setLogin(false);
                sessionManager.clearUserInfo();

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
            break;
            default:
                Toast.makeText(this, "Invalid operation", Toast.LENGTH_SHORT)
                        .show();
                break;
        }
        return false
                ;
    }
}
