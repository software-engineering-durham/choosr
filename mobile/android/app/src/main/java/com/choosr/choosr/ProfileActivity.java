package com.choosr.choosr;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.choosr.choosr.databinding.ActivityProfileBinding;
import com.choosr.choosr.models.User;
import com.choosr.choosr.utils.SessionManager;
import com.choosr.choosr.utils.WebServices;

public class ProfileActivity extends AppCompatActivity {

    private User user;
    private SessionManager sessionManager;
    private ActivityProfileBinding binding;
    private static final int EDIT_REQUEST_ACTIVITY = 102;
    public static final int PROFILE_EDITED = 202;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        binding = DataBindingUtil.setContentView(this,
                R.layout.activity_profile);

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserInfo();

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.setUser(user);
        Handlers handlers = new Handlers(this);
        binding.setHandlers(handlers);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class Handlers{
        private Context context;

        public Handlers(Context context) {
            this.context = context;
        }

        public void onFabClicked(View view) {
            Intent intent = new Intent(context, EditProfileActivity.class);
            startActivityForResult(intent, EDIT_REQUEST_ACTIVITY);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == EDIT_REQUEST_ACTIVITY && resultCode == PROFILE_EDITED){
            user = sessionManager.getUserInfo();
            binding.setUser(user);
        }
    }
}
