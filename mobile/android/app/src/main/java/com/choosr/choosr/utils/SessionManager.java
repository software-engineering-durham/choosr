package com.choosr.choosr.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.choosr.choosr.models.User;
import com.google.gson.Gson;

public class SessionManager {

    private static final String TAG = SessionManager.class.getSimpleName();

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "ChoosrLogin";
    private static final String KEY_LOGGED_IN = "IsLoggedIn";
    private static final String KEY_USER = "userkey";

    public SessionManager(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void setLogin(boolean isLoggedIn){
        editor.putBoolean(KEY_LOGGED_IN, isLoggedIn);

        editor.commit();

        Log.d(TAG, "User logged in");
    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(KEY_LOGGED_IN, false);
    }

    public void setUserInfo(User user) {
        Gson gson = new Gson();
        String userString = gson.toJson(user);

        editor.putString(KEY_USER, userString);

        editor.commit();

        Log.d(TAG, "User saved");
    }

    public User getUserInfo(){
        String userString = preferences.getString(KEY_USER, null);

        if(userString == null)
            return null;
        Gson gson = new Gson();
        return gson.fromJson(userString, User.class);
    }

    public void clearUserInfo(){
        editor.remove(KEY_USER);
        editor.commit();

        Log.d(TAG, "User removed");
    }
}
