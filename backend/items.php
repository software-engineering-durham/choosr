<?php
error_reporting(0);
include 'DBconn.php';
include 'categories.php';
include 'uploadimage.php';
//posted parameters
$action = $_POST['action'];
$userID = $_POST['userID']; 


//test data
//$userID = 1;
//$action = 'view';

switch($action){
	case 'view':
		showitems($conn,$userID);
		break;
	case 'find':
		finditembyID($conn,$userID);
		break;
	case 'viewavailable':
		showavailable($conn,$userID);
		break;	
	case 'viewdelivered':
		showdelivered($conn,$userID);
		break;
	case 'viewremoved':
		showremoved($conn,$userID);
		break;
	case 'remove':
		removeitem($conn,$userID);
		break;
	case 'add':
		additem($conn,$userID);
		break;
	case 'update':
		updateitem($conn,$userID);
		break;
	case 'setcas':
		setcategories($conn,$_POST['itemID']);
		break;
	case 'unbindca':
		unbindacategory($conn,$_POST['itemID']);
		break;
	case 'unbindallca':
		unbindcategories($conn,$_POST['itemID']);
		break;
	case 'addca':
		addnewcategory($conn);
		break;
    case 'removeca':
		removecategory($conn);
		break;
	default:
		echo "incorrect action";						
		
}




//show all rooms in the database
function showitems($conn,$userID){
	$responsecode = 0;
	$eachline = array();
    $response = array();
	$sql = "select * from Items where uploaderID = {$userID}";
	$result = $conn->query($sql);
	$i = 0;
	while($row = $result->fetch_assoc())
    {
		$eachline['item']= $row;
		$itemID = $row['itemID'];	
		$responsecode = 1;
		//show its categories
		$eachline['categories'] = showcategories($conn,$itemID);		
		$resultarr[$i++] = $eachline;
       
    }
	$response['success'] = $responsecode;
	$response['data'] = $resultarr;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}

function showavailable($conn,$userID){
	$responsecode = 0;
    $response = array();
	$sql = "select * from Items where uploaderID = {$userID} and status = 0";
	$result = $conn->query($sql);
	$i = 0;
	while($row = $result->fetch_assoc())
    {
		$eachline['item']= $row;
		$itemID = $row['itemID'];	
		$responsecode = 1;
		//show its categories
		$eachline['categories'] = showcategories($conn,$itemID);		
		$resultarr[$i++] = $eachline;
       
    }
	$response['success'] = $responsecode;
	$response['data'] = $resultarr;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}

function showremoved($conn,$userID){
	$responsecode = 0;
    $response = array();
	$sql = "select * from Items where uploaderID = {$userID} and status = 1";
	$result = $conn->query($sql);
	$i = 0;
	while($row = $result->fetch_assoc())
    {
		$eachline['item']= $row;
		$itemID = $row['itemID'];	
		$responsecode = 1;
		//show its categories
		$eachline['categories'] = showcategories($conn,$itemID);		
		$resultarr[$i++] = $eachline;
			
       
    }
	$response['success'] = $responsecode;
	$response['data'] = $resultarr;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}

function showdelivered($conn,$userID){
	$responsecode = 0;
    $response = array();
	$sql = "select * from Items where uploaderID = {$userID} and status = 2";
	$result = $conn->query($sql);
	$i = 0;
	while($row = $result->fetch_assoc())
    {
		$eachline['item']= $row;
		$itemID = $row['itemID'];	
		$responsecode = 1;
		//show its categories
		$eachline['categories'] = showcategories($conn,$itemID);		
		$resultarr[$i++] = $eachline;
			
       
    }
	$response['success'] = $responsecode;
	$response['data'] = $resultarr;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}

function finditembyID($conn,$userID){
	$responsecode = 0;
    $response = array();
	$itemID = $_POST['itemID'];
	
	$sql = "select * from Items where itemID = {$itemID}";
	$result = $conn->query($sql);
	while($row = $result->fetch_assoc())
    {
		$eachline['item']= $row;
		$responsecode = 1;
		//show its categories
		$eachline['categories'] = showcategories($conn,$itemID);		
    }
	$response['success'] = $responsecode;
	$response['data'] = $eachline;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
	
}

function removeitem($conn,$userID){
	$responsecode = 0;
    $response = array();
	$itemID = $_POST['itemID'];
	
	//$sql = "delete from Items where itemID = {$itemID}";
	$sql = "update Items set status = 1 where itemID = {$itemID}";
	//set this item "Removed"
	$result = $conn->query($sql);
	$response['data'] = 'error';
	if($result){
		$responsecode = 1;
		$response['data'] = 'The item is removed';
	}
	$response['success'] = $responsecode;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}

function additem($conn,$userID){
	$responsecode = 0;
    $response = array();
	//parameters
	$name = $_POST['name'];
	$quantity = $_POST['quantity'];
	$uploaderID = $userID;
	$bestbeforeDate = $_POST['bestbeforeDate'];
	$npicURL = transferURL($userID);//store picture and generate url address
	$calorie = $_POST['calorie'];
	$description = $_POST['description'];
	$lat = $_POST['lat'];
	$lon = $_POST['lon'];
	
	$sql = "insert into Items (name,quantity,uploaderID,bestbeforeDate,picURL,calorie,description, gmapLatitude, gmapLongitude) "
	."values('{$name}',{$quantity},{$uploaderID},'{$bestbeforeDate}','{$npicURL}',{$calorie},'{$description}', {$lat}, {$lon})";
	$result = $conn->query($sql);
	$response['data'] = 'error';
	if($result){
		$responsecode = 1;
		$response['data'] = 'The item was inserted';
		//add categories for it
		if(isset($_POST['categories'])){
			setcategories($conn,$conn->insert_id, false);			
		}
	}
	$response['success'] = $responsecode;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
}


function transferURL($userID){
	if(isset($_FILES['itempicture'])){
		$fp = uploadimage('users/'.$userID.'/items/', 'itempicture');
		return $fp;
	}
	return null;
}

function updateitem($conn,$userID){
	$responsecode = 0;
    $response = array();
	//parameters
	$itemID = $_POST['itemID'];
	$name = $_POST['name'];
	$quantity = $_POST['quantity'];
	$uploaderID = $userID;
	$bestbeforeDate = $_POST['bestbeforeDate'];
	$npicURL = transferURL($userID);//store picture and generate url address
	$calorie = $_POST['calorie'];
	$description = $_POST['description'];
	$lat = $_POST['lat'];
	$lon = $_POST['lon'];
	$sql = "";
	if($npicURL != null)
	{
	$sql = "update Items set name = '{$name}', quantity = {$quantity},uploaderID = {$uploaderID}, "
	."bestbeforeDate = '{$bestbeforeDate}', picURL = '{$npicURL}', calorie = {$calorie}, "
	."description = '{$description}', gmapLatitude = {$lat}, gmapLongitude = {$lon} where itemID = {$itemID}";
	}
	else{
		$sql = "update Items set name = '{$name}', quantity = {$quantity},uploaderID = {$uploaderID}, "
	."bestbeforeDate = '{$bestbeforeDate}', calorie = {$calorie}, "
	."description = '{$description}', gmapLatitude = {$lat}, gmapLongitude = {$lon} where itemID = {$itemID}";
	}
	$result = $conn->query($sql);
	$response['data'] = 'error';
	if($result){
		$responsecode = 1;
		$response['data'] = 'The item is updated';
	}
	$response['success'] = $responsecode;
	$conn->close();
	echo json_encode($response, JSON_NUMERIC_CHECK);
	
}
	


	



?>