<?php
	//error_reporting(0);
	include_once 'DBconn.php';
	include 'distance.php';
	include 'categories.php';
	
	$resultarr = array();
	
	//header("Content-Type: application/json");

	//posted parameters
	$receivedword = null;
	$radius = 20;
	$categories = null;
	$status = 0;
	$currentlat = 54.767392;
	$currentlon = -1.570303;
	$calories = 100000;

	if(isset($_POST['keyword'])){
		$receivedword = $_POST['keyword'];
	}
	if(isset($_POST['radius'])){
		$radius = floatval($_POST['radius']);
	}
	if(isset($_POST['categories'])){
		$categories = $_POST['categories'];
	}
	if(isset($_POST['lat'])){
		$currentlat = floatval($_POST['lat']);
	}
	if(isset($_POST['lon'])){
		$currentlon = floatval($_POST['lon']);
	}
	if(isset($_POST['status'])){
		$status = intval($_POST['status']);
	}
	if(isset($_POST['calories'])){
		$calories = intval($_POST['calories']);
	}

	$keyword = '%'.$receivedword."%";

	$responsecode = 0;
	$response = array();

	if($categories==null){
		$sql = "select Items.*, Users.username as uploaderusername, Users.image as uploaderpicture from Items inner join Users ON Users.userID = Items.uploaderID "
		."where name like '{$keyword}' and status = {$status} and calorie <= {$calories}";
	}
	else {
		$sql = "SELECT Items.*, Users.username as uploaderusername, Users.image as uploaderpicture FROM Items "
			."INNER JOIN Users ON Users.userID = Items.uploaderID "
			."INNER JOIN Item_category ON Items.itemID = Item_category.itemID "
			."WHERE Items.name like '{$keyword}' AND status = {$status} and calorie <= {$calories} "
			."AND Item_category.categoryID IN (".implode(",",$categories).") "
			."GROUP BY Items.itemID "
			."HAVING COUNT(DISTINCT Item_category.categoryID) = ".sizeof($categories);
			
	}
	
	$result = $conn->query($sql);
	while($row = $result->fetch_assoc())
	{
		$lat = $row['gmapLatitude'];
		$lon = $row['gmapLongitude'];
		$row['distance'] = round(earthDistance($currentlat, $currentlon, $lat, $lon) / 1000, 2);
		if($row['distance'] < $radius)
		{
			$r = array();
			$r['item'] = $row;
			$itemID = $row['itemID'];
			$r['categories'] = showcategories($conn,$itemID);
			$resultarr[] = $r;
			$responsecode = 1;	
		}
		
	}
	$response['success'] = $responsecode;
	$response['data'] = $resultarr;
	
	echo json_encode($response, JSON_NUMERIC_CHECK);
	$conn->close();

?>